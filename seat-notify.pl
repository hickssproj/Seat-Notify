#Darryl Hicks
#SP16 ICS 215 - Perl Basic Assignment
#Parse webpage - input class number => output seats available

#Is given a webpage file
my ($class, $fileName ) = @ARGV;
my $argSize = @ARGV;

#If number of arguments is not met, print out message and exit the program
open myFH, "<", "$fileName" or die "File doesn't exist\n";

#Puts file text into an array
@file = <myFH>;
close myFH;

my $i = 0;
my $ClassSeats = 0;

#Looks for the class name from the given Class Number
foreach ( @file ) {	
	if ( $_ =~ m!<TD NOWRAP CLASS="default">(\w+ $class)</TD>! ) {
		$ClassName = "$1"; 
		$ClassLine = $file[$i+9];
       	
       	#If there are multiple seats available it should add all available seats
       	if ( $ClassLine =~ m!<td class="default" align="center">(\d+)</td>!) {
       		
       		$ClassSeats += "$1";
       	}
       	            
	}
	$i++;
};

#If there are no seats available it should return an error message
if ($ClassSeats == 0){
	return "There are no seats for this class! Please try again!";	
}

#Returns number of available seats
return "$ClassSeats";


